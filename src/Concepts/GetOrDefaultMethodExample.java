package Concepts;

import java.util.HashMap;
import java.util.Map;

public class GetOrDefaultMethodExample {
	
	public static void main(String[] args) {
		
		Map<Integer,String> dataMap = new HashMap<>();
		dataMap.put(101,"Amod");
		dataMap.put(102,"Swati");
		dataMap.put(103,"Aaditya");
		dataMap.put(104,"Animesh");
		
		System.out.println(dataMap.get(101));
		System.out.println(dataMap.get(102));
		System.out.println(dataMap.get(103));
		System.out.println(dataMap.get(104));
		// Non-existing key in dataMap
		System.out.println(dataMap.get(105));
		System.out.println("===================================");
		System.out.println(dataMap.getOrDefault(101, "No Name"));
		System.out.println(dataMap.getOrDefault(102, "No Name"));
		System.out.println(dataMap.getOrDefault(103, "No Name"));
		System.out.println(dataMap.getOrDefault(104, "No Name"));
		System.out.println(dataMap.getOrDefault(105, "No Name"));
	
	}

}
