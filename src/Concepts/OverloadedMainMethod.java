package Concepts;

public class OverloadedMainMethod {
	
	// Overloaded main method
	public static void main(String str)
	{
		System.out.println("public static void main(String str)");
	}
	
	// Overloaded main method
	public static int main(int str)
	{
		System.out.println("public static void main(String str)");
		return 1;
	}
	
	// Overloaded main method
	public void main(String str, int val)
	{
		System.out.println("public void main(String str, int val)");
	}
	
	// Main method from where execution starts
	public static void main(String[] args) {
		System.out.println("public static void main(String[] args)");
		
		// Calling other main method
		main("Amod");
		main(10);
		new OverloadedMainMethod().main("Amod", 10);
	}

}
