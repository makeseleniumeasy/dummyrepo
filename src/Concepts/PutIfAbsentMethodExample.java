package Concepts;

import java.util.HashMap;
import java.util.Map;

public class PutIfAbsentMethodExample {
	
	public static void main(String[] args) {
		
		Map<Integer,String> dataMap = new HashMap<>();
		dataMap.put(101,"Amod");
		dataMap.put(102,"Swati");
		dataMap.put(103,"Aaditya");
		dataMap.put(104,"Animesh");
		
		// If we want to first check if a key is present and then add to map if not present
		if(!dataMap.containsKey(105))
			dataMap.put(105, "Suresh");
		
		// Or something as below
		if(dataMap.get(106) == null)
			dataMap.put(106, "Mahesh");
		
		System.out.println(dataMap);
		
		dataMap.putIfAbsent(107, "Sachin");
		dataMap.putIfAbsent(108, "Virat");
		
		System.out.println(dataMap);
	
	}

}
